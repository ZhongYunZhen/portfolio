import path from 'path'
import express from 'express'

const app = express()
const config = {
  port: process.env.PORT || '3000',
  hostname: process.env.HOST || 'localhost'
}

app.use('/', express.static('./public'))
app.use('*', (req, res, next) => {
  res.sendFile(path.resolve('./public', 'index.html'))
})

app.listen(config.port, config.hostname,  () => {
  console.log(`app running on hostname = ${config.hostname} & port = ${config.port}`)
})
