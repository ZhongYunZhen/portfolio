# Yun's Portfolio

This is a portfolio website project of Yun-Zhen Zhong

- Framework: [Next.JS](https://nextjs.org) + [TypeScript](https://www.typescriptlang.org)
- Custom Server: [Node Express](https://expressjs.com)
- Deployment: [Docker](https://www.docker.com) + [GitLab CI](https://docs.gitlab.com/ee/ci)
- Styling: [Styled Component](https://styled-components.com)
- ESlint: [Airbnb](https://github.com/airbnb/javascript)

# Overview

To view the [demo](https://portfolio.yuns.zdd997.com).

File Structure

- `src/assets/*` - the static assets including json data.
- `src/components/*` - the common-use react components
- `src/constants/*` - the constant value for the website
- `src/features/*` - the special-use react component for the website
- `src/utils/*` - the common-use util function
- `src/pages/*` - the page-related react component
- `src/styles/global.css` - default css rules for the website
- `src/styles/*` - the style related objects
- `src/types/global.d.ts` - the global type for website
- `src/types/custom/*` - the custom type for the website
- `src/static-source/*` - constant page data
- `server/*` - the custom server
- `public/*` - assets on public path

Others rule definitions

- Component

    always naming with an upper camel case when it's a file or `index.tsx` in a directory that fitted to the naming rule.

- Materials of a component
  - `*/${Name}/index.tsx` - the main component
  - `*/${Name}/components.tsx` - the related component for the main component
  - `*/${Name}/types.tsx` - the related type definitions
  - `*/${Name}/hooks.ts` - the related custom hook
  - `*/${Name}/styles.ts` - the related styled-component base components
  - `*/${Name}/utils.ts` - the related function
  - `*/${Name}/constants.ts` - the related constant value

# Running Locally

This application requires Node.js v10.2

run the following commands on you commandline/terminal

```cmd
$ git clone https://gitlab.com/ZhongYunZhen/portfolio.git yuns-portfolio
$ git cd yuns-portfolio
$ npm i
$ npm run dev
```
