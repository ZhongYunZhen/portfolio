FROM node:16.13.0-stretch-slim AS setter
WORKDIR /app
COPY ./server .

RUN npm install
RUN npm run build

FROM node:16.13.0-stretch-slim AS runner
WORKDIR /app
COPY ./out ./public
COPY ./server/package.json .
COPY --from=setter /app/node_modules ./node_modules
COPY --from=setter /app/dist/index.js .

EXPOSE 3000

CMD ["npm", "run", "start"]
