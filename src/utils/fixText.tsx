import React from 'react';

// Replace the string within * with bold formatting
export const fixText = (text: string) => {
  return text.split('*').map((txt, index) => {
    if (index % 2 === 0) {
      return <React.Fragment key={index}>{txt}</React.Fragment>;
    }
    return <strong key={index}>{txt}</strong>;
  });
};
