import Color from 'color';

export function mixColor(a: Color | undefined, b: Color | undefined, weight?: number) {
  const colorA = a instanceof Color ? a : Color(a);
  const colorB = b instanceof Color ? b : Color(b);
  return Color(colorA).mix(colorB, weight);
}
