/**
 * return a random number between min and max
 * @param max - The max number.
 * @param min - The min number.
 * @param acc - The accuracy.
 */
export const randomValue = (max: number, min: number = 0, acc?: number) => {
  const number = min + Math.random() * (max - min);
  if (acc !== undefined) {
    const pow = Math.pow(10, acc);
    return Math.round(number * pow) / pow;
  }
  return number;
};
