/**
 * return the input value straightly if it is not boolean type,
 * return the input valueOnTrue / valueOnFalse depence on true / false if it is boolean type
 *
 * ```js
 * // return: 'value for true'
 * booleanEscape(true, 'value for true', 'value for false')
 *
 * // return: 'value for false'
 * booleanEscape(false, 'value for true', 'value for false')
 *
 * // return: 'others'
 * booleanEscape('others', 'value for true', 'value for false')
 * ```
 */
export function booleanEscape<V, R extends V extends boolean ? never : V>(
  value: V,
  forTrue: R,
  forFalse?: R
): R | undefined {
  if (typeof value === 'boolean') return value ? forTrue : forFalse;
  return value as unknown as R;
}
