export const convertHexToRgb = (hexString?: string) => {
  if (!hexString) {
    return { r: 0, g: 0, b: 0 };
  }
  const [, r2, g2, b2, r1, g1, b1] = hexString.match(/#(..)(..)(..)|#(.)(.)(.)/);
  let hexR = r2;
  let hexG = g2;
  let hexB = b2;
  if (r1) {
    hexR = r1 + r1;
    hexG = g1 + g1;
    hexB = b1 + b1;
  }
  return {
    r: parseInt(hexR, 16),
    g: parseInt(hexG, 16),
    b: parseInt(hexB, 16),
  };
};
