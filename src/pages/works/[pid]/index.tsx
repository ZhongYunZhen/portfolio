import React, { useState } from 'react';
import { GetStaticPaths, GetStaticProps, NextPage } from 'next';
import { Column } from '@components/alignment/Column';
import { Row } from '@components/alignment/Row';
import { Breadcrumbs } from '@components/Breadcrumbs';
import { DisplayWall } from '@components/commons/DisplayWall';
import { TextButton } from '@components/form/Button';
import { THEME } from '@constants/theme';
import { DiamondText } from '@features/decoratedText/DiamondText';
import { PaddingBox } from '@components/layout/PaddingBox';
import { Header } from '@features/works/Header';
import { PageTitle } from '@features/decoratedText/PageTitle';
import { fixText } from '@utils/fixText';
import workData from '@static-source/works.json';
import * as ROUTES from '@constants/routes';

type Staff = string | { job: string; staff: string[] };
type ImageItem = {
  type: 'youtube' | 'image';
  origin: string;
  thumbnail: string;
};

type WorkProps = {
  imageList: ImageItem[];
  paths: { url: string; text: string }[];
  year: string;
  materials: string[];
  staff: Staff | Staff[];
  intro: string;
  tag: string;
  title: string;
  subtitle?: string;
  pathName: string;
  index: number;
};

const Work: NextPage<WorkProps> = ({
  title,
  subtitle,
  tag,
  pathName,
  imageList,
  year,
  materials,
  staff,
  intro,
  index,
}) => {
  const [position, setPosition] = useState<number>(0);

  return (
    <>
      <PageTitle title={title} subtitle={subtitle} />
      <Header>
        <Breadcrumbs
          path={[
            { url: ROUTES.HOME },
            { url: ROUTES.WORKS, text: 'WORKS' },
            { url: ROUTES.WORKS + '#' + tag, text: tag },
            { text: pathName, fill: true },
          ]}
          mainColor={THEME.COLOR.HIGHT_LIGHT}
          secondColor={THEME.COLOR.DECO_B}
        />
      </Header>
      <DisplayWall
        list={imageList.map(({ type, origin }) => ({ type, src: origin }))}
        position={position}
        width={860}
        height={460}
      />
      <DisplayWall.SelectBar
        list={imageList.map(({ thumbnail }) => thumbnail)}
        position={position}
        onPositionChange={setPosition}
      />
      <PaddingBox padding="1em 3em">
        <Column gap>
          <Row gap>
            <Column gap style={{ width: '100%' }}>
              <DiamondText color={THEME.COLOR.DECO_B}>創作年份 - Year</DiamondText>
              <PaddingBox paddingLeft="2.25em">{year}</PaddingBox>
            </Column>

            <Column gap style={{ width: '100%' }}>
              <DiamondText color={THEME.COLOR.DECO_B}>使用媒材 - Materials</DiamondText>
              <PaddingBox paddingLeft="2.25em">
                <Column gap>
                  {materials.map((material) => (
                    <div key={material}>{material}</div>
                  ))}
                </Column>
              </PaddingBox>
            </Column>
          </Row>

          <Column>
            <DiamondText color={THEME.COLOR.DECO_B}>製作成員 - Staff</DiamondText>
            <PaddingBox padding="1em 0 2em 2.25em">
              <Row wrap rowGap="2em">
                {renderStaff(staff)}
              </Row>
            </PaddingBox>
          </Column>
          <div />

          {/* intro */}
          <Column gap>
            <DiamondText color={THEME.COLOR.DECO_B}>介紹 - Intro</DiamondText>
            <PaddingBox padding="0 0 0 2.25em">
              <div style={{ whiteSpace: 'break-spaces', lineHeight: '2em' }}>{fixText(intro)}</div>
            </PaddingBox>
          </Column>
          <PaddingBox padding="2em 0 0">
            <Row justify="space-between">
              <DiamondText fill={false} color={THEME.COLOR.DECO_A}>
                <TextButton href={'/works/' + Object.keys(workData)[index - 1]}>
                  上一個作品 - prev
                </TextButton>
              </DiamondText>
              <DiamondText fill={false} color={THEME.COLOR.DECO_A}>
                <TextButton href={'/works/' + Object.keys(workData)[index + 1]}>
                  下一個作品 - next
                </TextButton>
              </DiamondText>
            </Row>
          </PaddingBox>
        </Column>
      </PaddingBox>
    </>
  );
};

export default Work;

const renderStaff = (staff: Staff | Staff[]) => {
  if (Array.isArray(staff)) {
    return staff.map((data, i) => <React.Fragment key={i}>{renderStaff(data)}</React.Fragment>);
  }
  if (typeof staff === 'string') {
    return <>{staff}</>;
  }
  return (
    <Column style={{ width: '50%' }} gap>
      <div>。{staff.job}</div>
      <PaddingBox paddingLeft="1em">{fixText(staff.staff.join(', '))}</PaddingBox>
    </Column>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: Object.keys(workData).map((pid) => ({ params: { pid } })),
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps = async (context) => {
  const pathName = context.params.pid;
  if (Array.isArray(pathName)) {
    return { props: {} };
  }
  const index = Object.keys(workData).indexOf(pathName);
  const { imageList: oriImageList, ...data } = workData[pathName];
  const imageList = oriImageList.map(({ type, imageName, url }) => {
    if (type === 'youtube') {
      return { type, origin: url, thumbnail: '/pic/small/play-03.png' };
    }
    return { type, origin: '/pic/' + imageName, thumbnail: '/pic/small/' + imageName };
  });

  return { props: { imageList, pathName, index, ...data } };
};
