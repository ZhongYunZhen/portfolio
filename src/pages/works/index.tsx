import React from 'react';
import { NextPage } from 'next';
import { TitleBlock } from '@components/layout/TitleBlock';
import { PageTitle } from '@features/decoratedText/PageTitle';
import { WorkItem } from '@features/works/WorkItem';
import { WorksContainer } from '@features/works/WorksContainer';
import workData from '@static-source/works.json';
import * as ROUTES from '@constants/routes';

type Staff = string | { job: string; staff: string[] };
type ImageItem = {
  type: 'youtube' | 'image';
  origin: string;
  thumbnail: string;
};
type DisplayItem = {
  title: string;
  subtitle: string;
  thumbnail: string;
  imageList: ImageItem[];
  tag: string[];
  year: string;
  materials: string[];
  staff: Staff | Staff[];
  path: string;
};

const SORT_CONDITION = 'tag';

const DISPLAY_ITEMS = (() => {
  const tmpObj = {};
  Object.keys(workData).map((name) => {
    if (!tmpObj[workData[name][SORT_CONDITION]]) {
      tmpObj[workData[name][SORT_CONDITION]] = [];
    }
    tmpObj[workData[name][SORT_CONDITION]].push({ ...workData[name], path: name });
  });
  return tmpObj;
})();

const Works: NextPage = () => {
  return (
    <>
      <PageTitle title="作品" subtitle="Works" />
      {Object.keys(DISPLAY_ITEMS).map((title, i) => (
        <TitleBlock
          key={i}
          title={title}
          titlePath={[
            { url: ROUTES.WORKS },
            { url: ROUTES.WORKS + '#' + title, text: title, fill: true },
          ]}
        >
          <WorksContainer>
            {DISPLAY_ITEMS[title].map(
              ({ thumbnail, title, year, materials, path }: DisplayItem, j: number) => (
                <WorkItem
                  key={j}
                  href={ROUTES.WORKS + '/' + path}
                  imageSrc={thumbnail}
                  title={title}
                  year={year}
                  contents={materials.join(', ')}
                />
              )
            )}
          </WorksContainer>
        </TitleBlock>
      ))}
    </>
  );
};
export default Works;
