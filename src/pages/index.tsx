import { FilledImage } from '@components/layout/FilledImage';
import { LayoutType } from '@enums/LayoutType';
import { HomeLayout } from '@features/HomeLayout';
import { FloatingButton } from '@features/home/FloatingButton';
import { THEME } from '@constants/theme';
import * as ROUTES from '@constants/routes';

const DISPLAY_ITEMS = [
  { href: ROUTES.ABOUT, size: 250, src: '/pic/me.png' },
  {
    href: ROUTES.GALLERY,
    text: 'GALLERY',
    posX: ['250px', '80px'],
    posY: ['100px', '180px'],
    size: 150,
    color: THEME.COLOR.DECO_A,
    fill: true,
  },
  {
    href: ROUTES.WORKS,
    text: 'WORKS',
    posX: ['-250px', '-100px'],
    posY: ['-100px', '-150px'],
    size: 150,
    color: THEME.COLOR.BODY,
    textColor: THEME.COLOR.DECO_A,
    fill: true,
  },
];

const Home = () => {
  return (
    <HomeLayout>
      {DISPLAY_ITEMS.map(({ text, src, ...data }, i) => (
        <FloatingButton key={i} {...data}>
          {src && <FilledImage src={src}></FilledImage>}
          {text && text}
        </FloatingButton>
      ))}
    </HomeLayout>
  );
};

export default Home;

Home.layout = LayoutType.NO_LAYOUT;
