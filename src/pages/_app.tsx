import React from 'react';
import { AppProps } from 'next/dist/next-server/lib/router/router';
import { AppLayout } from '@features/AppLayout';
import { CustomNextPage } from '@/types/custom/CustomNextPage';
import { LayoutType } from '@enums/LayoutType';
import '@/styles/globals.css';

interface AppWithLayoutProps extends Omit<AppProps, 'Component'> {
  Component: CustomNextPage;
}

function MyApp({ Component, pageProps }: AppWithLayoutProps) {
  const { layout } = Component;

  return (
    <Layout type={layout}>
      <Component {...pageProps} />
    </Layout>
  );
}

export default MyApp;

// resolve the feature of layout customization
const Layout: React.FC<React.PropsWithChildren<{ type: CustomNextPage['layout'] }>> = ({
  type,
  children,
}) => {
  switch (type) {
    // use the default layout when the layout property is not setting up
    case undefined:
    case LayoutType.DEFAULT:
      return <AppLayout>{children}</AppLayout>;

    case LayoutType.NO_LAYOUT:
      return <>{children}</>;

    // use the customize layout when the layout property is a component
    default: {
      const CustomLayout = type;
      return <CustomLayout>{children}</CustomLayout>;
    }
  }
};
