import React, { useState } from 'react';
import { PageTitle } from '@features/decoratedText/PageTitle';
import { TitleBlock } from '@components/layout/TitleBlock';
import { Row } from '@components/alignment/Row';
import { GalleryItem } from '@features/gallery/GalleryItem';
import { PaddingBox } from '@components/layout/PaddingBox';
import { ShowImage } from '@features/gallery/ShowImage';
import galleryData from '@static-source/gallery.json';
import * as ROUTES from '@constants/routes';

const SORT_CONDITION = 'type';
const DISPLAY_ITEMS = (() => {
  const tmpObj = {};
  galleryData.map((data, index) => {
    const key = data[SORT_CONDITION];
    if (!tmpObj[key]) tmpObj[key] = [];

    tmpObj[key].push({ ...data, index });
  });
  return tmpObj;
})();

const Gallery: React.FC<{}> = () => {
  const [showImage, setShowImage] = useState<false | number>(false);

  return (
    <>
      <PageTitle title="畫廊" subtitle="Gallery" />
      {Object.keys(DISPLAY_ITEMS).map((title, i) => (
        <TitleBlock
          key={i}
          title={title}
          titlePath={[{}, { url: ROUTES.GALLERY + '#' + title, text: title, fill: true }]}
        >
          <PaddingBox padding="2em">
            <Row wrap gap rowGap>
              {DISPLAY_ITEMS[title].map(({ url, index }) => (
                <GalleryItem key={url} url={url} onClick={() => setShowImage(index)} />
              ))}
            </Row>
          </PaddingBox>
        </TitleBlock>
      ))}
      {showImage !== false && (
        <ShowImage
          onHidden={() => setShowImage(false)}
          showImage={showImage}
          imageList={galleryData.map(({ url }) => url)}
        />
      )}
    </>
  );
};

export default Gallery;
