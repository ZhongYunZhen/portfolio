import React from 'react';
import { NextPage } from 'next';
import { PageTitle } from '@features/decoratedText/PageTitle';
import { DiamondLine } from '@features/about/DiamondLine';
import { TitleBlock } from '@components/layout/TitleBlock';
import { PaddingBox } from '@components/layout/PaddingBox';
import { fixText } from '@utils/fixText';
import { SkillBlock } from '@features/about/SkillBlock';
import { Diamond } from '@components/Shapes/Diamond';
import { Row } from '@components/alignment/Row';
import aboutData from '@static-source/about.json';

const TITLE_SIZE = '1.25em';

const About: NextPage = () => {
  return (
    <>
      <PageTitle title="關於" subtitle="About" />
      <PaddingBox padding="3em 1em">
        <Row align="center" justify="space-evenly">
          <Diamond size={300} background="center / cover url(/pic/me3.jpg)" />
          <PaddingBox padding="0 1em" style={{ lineHeight: 2, fontSize: '1.1em' }}>
            嗨咿～我是鍾昀蓁 <br />
            畢業於臺北藝術大學 新媒體藝術學系 <br />
            興趣是畫畫、吸狗、睡覺 <br />
            喜歡的東西是甜品、Puly以及大海 <br />
            10:10的狗派(ˊ▽ˋ) <br />
          </PaddingBox>
        </Row>
      </PaddingBox>
      <PaddingBox paddingLeft="20%">
        <TitleBlock
          fontSize={TITLE_SIZE}
          title="autobiography"
          titlePath={[
            {},
            { text: '自傳 - Autobiography', fill: false, url: '/about/#autobiography' },
          ]}
        >
          <PaddingBox paddingLeft="0.8em">
            {aboutData.intro.map(
              ({ type, text }: { type: 'normal' | 'dotted'; text: string }, i) => (
                <DiamondLine type={type} color="#e2cf9f" key={i}>
                  {text}
                </DiamondLine>
              )
            )}
          </PaddingBox>
        </TitleBlock>
      </PaddingBox>
      <PaddingBox padding="1em 0">
        <TitleBlock
          fontSize={TITLE_SIZE}
          mainColor="#e2cf9f"
          secondColor="#3e4b56"
          title="experience"
          titlePath={[{}, { text: '經歷 - Experience', fill: true, url: '/about/#experience' }]}
        >
          {aboutData.experience.map((txt, i) => (
            <p key={i}>{fixText(txt)}</p>
          ))}
        </TitleBlock>
      </PaddingBox>
      <PaddingBox>
        <SkillBlock tools={aboutData.tools} fontSize={TITLE_SIZE} skills={aboutData.skills} />
      </PaddingBox>
    </>
  );
};

export default About;
