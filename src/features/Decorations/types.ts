export type Item = {
  posX: number;
  posY: number;
  size: number;
  speed?: number;
  fill?: boolean;
  color?: number | string;
  targetPos?: { x: number; y: number };
};
