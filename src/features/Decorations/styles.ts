import styled from 'styled-components';

export const Container = styled.div((props) => ({
  pointerEvents: 'none',
  height: '100%',
  width: '100%',
  position: 'fixed',
  tpp: 0,
  zIndex: 101,
}));
