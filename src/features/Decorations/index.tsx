import React, { useRef } from 'react';
import Sketch from 'react-p5';
import p5Types from 'p5';
import { randomValue } from '@utils/randomValue';
import * as Styles from './styles';
import * as Types from './types';
import * as CONSTANTS from './constants';
import * as utils from './utils';

interface DecorationsProps {
  density?: number;
  itemSize?: [min: number, max: number];
  itemColor?: string[];
  getEnableSize: () => number;
}

const Decorations: React.FC<DecorationsProps> = ({
  density = CONSTANTS.DENSITY,
  itemSize = CONSTANTS.ITEM_SIZE,
  itemColor = CONSTANTS.ITEM_COLOR,
  getEnableSize,
}) => {
  const itemList = useRef<Types.Item[]>();
  const delta = useRef<number>(0);
  const windowTop = useRef<number>(0);
  const mouseItem = useRef<Types.Item | undefined>();
  const enableSize = useRef<number>(0);
  const itemColorCount = useRef<number>(0);

  const setup = (p5: p5Types, canvasParentRef: Element) => {
    enableSize.current = getEnableSize();
    windowTop.current = window.scrollY;

    const height = window.innerHeight;
    const width = enableSize.current;
    const count = Math.ceil(width * height * density * 2);
    itemColorCount.current = count;

    itemList.current = new Array(count).fill('').map((data, i) => {
      const itemData = utils.generateItemData(width, height, true, itemSize, i % 2 === 0);
      return {
        ...itemData,
        color: i,
        fill: i % 5 < 4,
      };
    });
    p5.createCanvas(window.innerWidth, height).parent(canvasParentRef);
  };

  const draw = (p5: p5Types) => {
    p5.clear();
    delta.current += (windowTop.current - window.scrollY) * 4;
    delta.current--;

    itemList.current.map((itemData, i) => {
      utils.generateShape(p5, itemData, itemList.current[i], itemColor);
      let resetPosY = undefined;

      // if overflow canvas
      if (itemData.posY > p5.windowHeight) resetPosY = -itemSize[1] * 1.5;
      if (itemData.posY < -itemSize[1] * 1.5) resetPosY = p5.windowHeight;

      if (resetPosY) {
        itemList.current[i] = {
          ...itemList.current[i],
          ...utils.generateItemData(
            enableSize.current,
            resetPosY,
            false,
            itemSize,
            itemData.posX > p5.width / 2
          ),
        };
      }

      itemList.current[i].posY += itemData.speed * delta.current;
    });

    if (mouseItem.current) {
      if (mouseItem.current.size <= itemSize[1]) {
        mouseItem.current.size++;
      }
      const { posY, size, ...data } = mouseItem.current;
      utils.generateShape(
        p5,
        { ...data, size, posY: posY - size / 2 },
        mouseItem.current,
        itemColor
      );
    }

    delta.current = 0;
    windowTop.current = window.scrollY;
  };

  const windowResized = (p5: p5Types) => {
    const delta = p5.width - p5.windowWidth;
    const currentEnableSize = getEnableSize();
    const enableSizeDelta = enableSize.current - currentEnableSize;
    itemList.current.map(({ posX }, i) => {
      if (posX > p5.width / 2) {
        itemList.current[i].posX -= delta - enableSizeDelta;
        return;
      }
      itemList.current[i].posX -= enableSizeDelta;
    });
    p5.resizeCanvas(p5.windowWidth, p5.windowHeight);
    enableSize.current = currentEnableSize;
  };

  const mousePressed = (p5: p5Types) => {
    if (!utils.checkIfInEnableArea(p5, enableSize.current)) return;
    const color = itemColorCount.current++;
    mouseItem.current = {
      posX: p5.mouseX,
      posY: p5.mouseY,
      speed: randomValue(0.5, 0.3),
      color,
      size: 0,
    };
  };

  const mouseReleased = () => {
    if (!mouseItem.current) return;
    const { posY, size, targetPos, ...data } = mouseItem.current;
    itemList.current.push({ ...data, size, posY: posY - size / 2 });
    mouseItem.current = undefined;
  };

  const mouseDragged = (p5: p5Types) => {
    if (!mouseItem.current || !utils.checkIfInEnableArea(p5, enableSize.current)) return;
    mouseItem.current.targetPos = { x: p5.mouseX, y: p5.mouseY };
  };

  return (
    <Styles.Container>
      <Sketch
        setup={setup}
        draw={draw}
        windowResized={windowResized}
        mousePressed={mousePressed}
        mouseReleased={mouseReleased}
        mouseDragged={mouseDragged}
      />
    </Styles.Container>
  );
};

export default Decorations;
