import { THEME } from '@constants/theme';

export const ITEM_SIZE: [min: number, max: number] = [20, 50];
export const ITEM_COLOR = [THEME.COLOR.DECO_A, THEME.COLOR.DECO_B, THEME.COLOR.DECO_C];
export const DENSITY = 0.0002;
