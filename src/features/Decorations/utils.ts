import p5Types from 'p5';
import { randomValue } from '@utils/randomValue';
import * as Types from './types';
import * as CONSTANTS from './constants';

export const generateItemData = (
  width: number,
  height: number,
  randomY: boolean = true,
  sizeRange: [min: number, max: number],
  alignRight?: boolean
): Types.Item => ({
  posX: alignRight
    ? randomValue(window.innerWidth, window.innerWidth - width) - sizeRange[1]
    : randomValue(width, 0) + sizeRange[1],
  posY: randomY ? randomValue(height, 0) : height,
  size: randomValue(0, 1) < 0.3 ? randomValue(...sizeRange) : randomValue(5, sizeRange[0]),
  speed: randomValue(0.3, 0.2),
});

export const generateShape = (
  p5: p5Types,
  itemData: Types.Item,
  item: Types.Item,
  colorArr: string[]
) => {
  const { color, posX, posY, size, fill, targetPos } = itemData;

  if (targetPos) {
    item.posX = easingNumber(item.posX, targetPos.x);
    item.posY = easingNumber(item.posY, targetPos.y);
  }
  p5.push();
  if (fill) {
    p5.noStroke();
    p5.fill(getColor(color, colorArr) + 'cc');
  } else {
    p5.stroke(getColor(color, colorArr) + 'cc');
    p5.fill(0, 0);
  }
  p5.translate(posX, posY);
  p5.rotate(p5.radians(45));
  p5.rect(0, 0, size, size);
  p5.pop();
};

const easingNumber = (ori: number, target: number, rate: number = 0.1) => {
  return ori + (target - ori) * rate;
};

const getColor = (color: string | number, colorArr: string[] = []) => {
  return typeof color === 'number' ? colorArr[color % colorArr.length] : color;
};

export const checkIfInEnableArea = (p5: p5Types, enableSize: number) => {
  const mouseX = p5.mouseX;
  if (mouseX > enableSize && mouseX < p5.width - enableSize) {
    return false;
  }
  return true;
};
