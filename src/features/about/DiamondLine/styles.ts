import { Diamond } from '@components/Shapes/Diamond';
import styled from 'styled-components';

export const TextContainer = styled.div.withConfig({})<{ borderStyle: string }>((props) => ({
  position: 'relative',
  borderLeft: `${props.borderStyle} 3px ${props.color}`,
  padding: '1em',
  lineHeight: '2em',
  whiteSpace: 'pre-wrap',
}));

export const PointDiamond = styled(Diamond).withConfig({})<{ size: string }>((props) => ({
  position: 'absolute',
  left: `calc(-${+props.size.split('em')[0] / 2}em - 1.5px)`,
  top: '1.25em',
}));
