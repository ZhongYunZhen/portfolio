import React from 'react';
import { THEME } from '@constants/theme';
import { fixText } from '@utils/fixText';
import * as Styles from './styles';

interface DiamondLineProps extends React.HTMLAttributes<HTMLDivElement> {
  type: 'normal' | 'dotted';
  color: string;
  children: string;
}

export const DiamondLine: React.FC<DiamondLineProps> = ({ type, color, children }) => {
  return (
    <Styles.TextContainer borderStyle={type === 'normal' ? 'solid' : 'dashed'} color={color}>
      <Styles.PointDiamond
        size="1.5em"
        color={color}
        fill={type === 'normal'}
        background={THEME.COLOR.BODY}
        borderWidth="2px"
      />
      {fixText(children)}
    </Styles.TextContainer>
  );
};
