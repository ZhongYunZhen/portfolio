import React, { useState } from 'react';
import { Column } from '@components/alignment/Column';
import { Row } from '@components/alignment/Row';
import { PaddingBox } from '@components/layout/PaddingBox';
import { TitleBlock } from '@components/layout/TitleBlock';
import { THEME } from '@constants/theme';
import { DiamondText } from '@features/decoratedText/DiamondText';
import { SkillCanvas } from './SkillCanvas';

interface SkillBlockProps {
  fontSize: string;
  skills: { title: string; describe: string; tools: string[] }[];
  tools: { text: string; proficiency: number }[];
}

export const SkillBlock: React.FC<SkillBlockProps> = ({ fontSize, skills, tools }) => {
  const [focusTools, setFocusTools] = useState<string[]>([]);

  return (
    <Row gap>
      <div style={{ width: '50%' }}>
        <SkillCanvas tools={tools} focusTools={focusTools} />
      </div>
      <TitleBlock
        style={{ width: '50%' }}
        fontSize={fontSize}
        mainColor="#ffee8a"
        secondColor="#9bce8d"
        title="skills"
        titlePath={[{}, { text: '專業技能 - Skills', fill: true, url: '/about/#skills' }]}
      >
        <PaddingBox paddingLeft="1em" style={{ lineHeight: 2 }}>
          <Column gap>
            {skills.map(({ title, describe, tools }) => (
              <Column key={title} onClick={() => setFocusTools(tools)}>
                <DiamondText fill={false} color={THEME.COLOR.TEXT} fontSize="1.17em">
                  {title}
                </DiamondText>
                <PaddingBox paddingLeft="2em">{describe}</PaddingBox>
              </Column>
            ))}
          </Column>
        </PaddingBox>
      </TitleBlock>
    </Row>
  );
};
