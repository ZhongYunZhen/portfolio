import styled from 'styled-components';

type FloatingTextProps = {
  fontSize?: string | number;
  color?: string;
  position?: string;
  duration?: number;
  zIndex?: number;
  opacity?: number;
};

export const FloatingText = styled.div
  .attrs<FloatingTextProps>((props) => ({
    style: {
      opacity: props.opacity,
      transform: `translate(${props.position})`,
      transition: `transform linear ${props.duration}s`,
    },
  }))
  .withConfig({
    shouldForwardProp: (props) =>
      !['fontSize', 'color', 'position', 'duration', 'zIndex', 'opacity'].includes(props),
  })<FloatingTextProps>((props) => ({
  display: 'inline-block',
  position: 'absolute',
  fontSize: props.fontSize,
  fontWeight: 'bolder',
  zIndex: props.zIndex,
  filter: 'drop-shadow(2px 2px #fff)',
}));
