import React, { useEffect, useRef, useState } from 'react';
import { THEME } from '@constants/theme';
import { mixColor } from '@utils/mixColor';
import * as Styles from './styles';

type DisplayItem = {
  text: string;
  color: string;
  fontSize: string;
  posX: number;
  posY: string | number;
  speed: number;
  normalColor: string;
};

export const SkillCanvas: React.FC<{
  tools: { text: string; proficiency: number }[];
  focusTools: string[];
}> = ({ tools, focusTools }) => {
  const [canvasSize, setCanvasSize] = useState<{ width: number; height: number }>({
    width: 0,
    height: 0,
  });
  const [displayArray, setDisplayArray] = useState<DisplayItem[]>([]);
  const ref = useRef<any>();
  useEffect(() => {
    if (!tools || displayArray.length > 0 || canvasSize.width === 0) return;
    const count = {};
    setDisplayArray(
      tools.map(({ text, proficiency }, i) => {
        const speedBase = Math.ceil((Math.random() + 1) * 5);
        const color = mixColor('transparent', THEME.COLOR.TEXT, proficiency / 20);
        if (!count[speedBase]) count[speedBase] = 0;
        count[speedBase]++;
        return {
          text,
          fontSize: proficiency / 2 + 'em',
          posX: (Math.random() - 0.1) * canvasSize.width * 1.2,
          posY: (Math.random() * 0.1 + count[speedBase] / 10) * canvasSize.height,
          color,
          normalColor: color,
          speed: speedBase * 5,
          zIndex: speedBase,
        };
      })
    );
  }, [tools, canvasSize]);

  useEffect(() => {
    if (displayArray.length === 0) return;
    setDisplayArray((displayArray) =>
      displayArray.map((data) => ({
        ...data,
        color: focusTools.includes(data.text)
          ? mixColor(THEME.COLOR.DECO_B, THEME.COLOR.HIGHT_LIGHT, Math.random())
          : data.normalColor,
      }))
    );
  }, [focusTools]);

  useEffect(() => {
    const func = () => {
      setCanvasSize({ width: ref.current.offsetWidth, height: ref.current.offsetHeight });
    };
    func();
    window.addEventListener('resize', func);
    return () => {
      window.removeEventListener('resize', func);
    };
  }, []);

  return (
    <div
      ref={ref}
      style={{ width: '100%', height: '100%', overflow: 'hidden', position: 'relative' }}
    >
      {displayArray.map(({ text, ...data }) => (
        <FloatingText key={text} canvasSize={canvasSize} {...data}>
          {text}
        </FloatingText>
      ))}
    </div>
  );
};

type FloatingTextProps = {
  canvasSize: { width: number; height: number };
  children?: string;
  color: string;
  fontSize: string;
  posX?: number;
  posY?: string | number;
  speed?: number;
};

type TransformProp = {
  posX: number;
  posY: string | number;
  duration: number;
  opacity: number;
};

const FloatingText: React.FC<FloatingTextProps> = ({
  children,
  canvasSize,
  posX = 0,
  posY = 0,
  speed,
  ...props
}) => {
  const [transform, setTransform] = useState<TransformProp>({
    posX,
    posY,
    duration: 0,
    opacity: 0,
  });
  const ref = useRef<HTMLDivElement>();

  const handleTransitionEnd: React.TransitionEventHandler<HTMLDivElement> = () => {
    if (transform.posY !== '-100%') {
      const width = ref.current.offsetWidth;
      setTransform({
        posX: Math.random() * (canvasSize.width - width),
        posY: '-100%',
        duration: 1,
        opacity: 0,
      });
      return;
    }
    const posY = canvasSize.height;
    setTransform(({ posX }) => ({ posX, posY, duration: posY, opacity: 1 }));
    return;
  };

  useEffect(() => {
    const posY = canvasSize.height;
    const transform = getComputedStyle(ref.current).transform;
    let duration = posY;
    const value = transform.match(/\, ([\d.]+)\)/);
    if (value) {
      const [, top] = value;
      duration = posY - +top;
    }

    setTransform(({ posX }) => ({ posX, posY, duration, opacity: 1 }));
  }, [canvasSize]);

  return (
    <Styles.FloatingText
      ref={ref}
      onTransitionEnd={handleTransitionEnd}
      position={`${convertValue(transform.posX)}, ${convertValue(transform.posY)}`}
      duration={transform.duration / speed}
      opacity={transform.opacity}
      {...props}
    >
      {children}
    </Styles.FloatingText>
  );
};

const convertValue = (value: string | number) => {
  if (typeof value === 'string') {
    return value;
  }
  return value + 'px';
};
