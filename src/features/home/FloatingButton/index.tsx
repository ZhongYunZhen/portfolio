import React from 'react';
import { LinkButton } from '@components/form/Button';
import { Diamond, DiamondProps } from '@components/Shapes/Diamond';
import * as Styles from './styles';

interface FloatingButtonProps extends DiamondProps {
  href: string;
  posX?: string[];
  posY?: string[];
  size: string | number;
}

export const FloatingButton: React.FC<FloatingButtonProps> = ({
  href,
  posX = ['0', '0'],
  posY = ['0', '0'],
  size,
  children,
  ...props
}) => {
  return (
    <Styles.Container posX={posX} posY={posY}>
      <Diamond {...props} size={size}>
        <LinkButton href={href}>{children}</LinkButton>
      </Diamond>
    </Styles.Container>
  );
};
