import styled from 'styled-components';

export const Container = styled.div<{ posX: string[]; posY: string[] }>((props) => ({
  position: 'absolute',
  fontSize: '1.25em',
  transform: `translate(${props.posX[1]}, ${props.posY[1]})`,
  '@media (min-width: 980px)': {
    transform: `translate(${props.posX[0]}, ${props.posY[0]})`,
  },
}));
