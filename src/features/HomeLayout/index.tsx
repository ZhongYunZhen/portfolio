import dynamic from 'next/dynamic';
import React from 'react';
import { THEME } from '@constants/theme';
import { Icons } from '@components/layout/Header';
import * as Styles from './styles';

const Decorations = dynamic(() => import('@features/Decorations'), { ssr: false });

export const HomeLayout: React.FC<React.PropsWithChildren<{}>> = ({ children }) => {
  return (
    <Styles.Container mainColor={THEME.COLOR.BODY} secondColor={THEME.COLOR.DECO_A}>
      <Icons />
      <Decorations
        density={0.0001}
        getEnableSize={() => (window.innerWidth / 5) * 2}
        itemColor={[
          THEME.COLOR.DECO_A,
          THEME.COLOR.DECO_B,
          THEME.COLOR.DECO_C,
          THEME.COLOR.BODY,
          THEME.COLOR.MENU,
        ]}
      />
      {children}
    </Styles.Container>
  );
};
