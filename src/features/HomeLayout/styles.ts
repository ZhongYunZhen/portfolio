import styled from 'styled-components';

export const Container = styled.div.withConfig({
  shouldForwardProp: (props) => !['mainColor', 'secondColor'].includes(props),
})<{ mainColor: string; secondColor: string }>((props) => ({
  height: '100vh',
  width: '100vw',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  position: 'relative',
  '&::before, &::after': {
    zIndex: -1,
    content: "''",
    position: 'absolute',
    inset: 0,
  },
  '::after': {
    backgroundColor: props.secondColor,
    transition: 'ease .2s',
    clipPath: 'polygon(0 0, 100% 0, 100% 60%, 0 40%, 0 0)',
    '@media (min-width: 980px)': {
      clipPath: 'polygon(0 0, 60% 0, 40% 100%, 0 100%, 0 0)',
    },
  },
  '::before': {
    backgroundColor: props.mainColor,
  },
}));
