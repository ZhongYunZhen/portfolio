import React from 'react';
import { unit, UnitType } from '@styles/css/unit';
import * as Styles from './styles';

export interface DiamondProps extends React.HTMLAttributes<HTMLDivElement> {
  color?: string;
  fill?: boolean;
  size?: UnitType;
  left?: UnitType;
  top?: UnitType;
}

export const Diamond: React.FC<DiamondProps> = ({
  color,
  fill,
  size,
  left,
  top,
  children,
  ...props
}) => {
  return (
    <Styles.Container
      color={fill ? '#fff' : color}
      left={unit(left)}
      top={unit(top)}
      size={unit(size)}
      {...props}
    >
      {children && <Styles.Content>{children}</Styles.Content>}
      <Styles.Diamond color={color} fill={fill} />
    </Styles.Container>
  );
};
