import styled, { css } from 'styled-components';

type ContainerProps = {
  color?: string;
  size?: string;
  padding?: string;
  left?: string;
  top?: string;
};

export const Container = styled('div').withConfig({
  shouldForwardProp: (props) => !['color', 'size', 'padding', 'left', 'top'].includes(props),
})<ContainerProps>((props) => ({
  position: 'absolute',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  color: props.color,
  width: props.size,
  height: props.size,
  padding: props.padding,
  left: props.left,
  top: props.top,
}));

export const Content = styled.div`
  z-index: 2;
`;

type DiamondProps = {
  color?: string;
  fill?: boolean;
};

export const Diamond = styled.div.withConfig({
  shouldForwardProp: (props) => !['color', 'fill'].includes(props),
})<DiamondProps>((props) => ({
  transform: 'rotate(45deg)',
  position: 'absolute',
  inset: 0,
  color: props.color,
  backgroundColor: props.fill ? 'currentColor' : undefined,
  border: props.fill ? 'solid 1px currentColor' : undefined,
}));
