import { Row } from '@components/alignment/Row';
import styled from 'styled-components';

export const WorksContainer = styled(Row)(() => ({
  margin: 'auto',
  width: '100%',
  padding: '2em 0 5em',
  '@media (min-width: 500px)': {
    width: 410,
  },
  '@media (min-width: 980px)': {
    width: 820,
  },
}));
