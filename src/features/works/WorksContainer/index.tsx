import React from 'react';
import * as Styles from './styles';

interface WorksContainerProps extends React.HTMLAttributes<HTMLDivElement> {}

export const WorksContainer: React.FC<WorksContainerProps> = ({ children }) => {
  return (
    <Styles.WorksContainer gap wrap rowGap="2em">
      {children}
    </Styles.WorksContainer>
  );
};
