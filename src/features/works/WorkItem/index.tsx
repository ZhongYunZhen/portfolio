import React from 'react';
import Link from 'next/link';
import { Column } from '@components/alignment/Column';
import { FilledImage } from '@components/layout/FilledImage';
import { THEME } from '@constants/theme';
import * as Styles from './styles';

interface WorkItemProps extends React.HTMLAttributes<HTMLDivElement> {
  imageSrc: string;
  title: string;
  contents: string;
  year: string;
  href: string;
}

export const WorkItem: React.FC<WorkItemProps> = ({ href, imageSrc, title, contents, year }) => {
  return (
    <Link href={href}>
      <Styles.Container gap>
        <Styles.DiamondContainer
          size={150}
          borderColor={THEME.COLOR.DECO_B}
          color={THEME.COLOR.GRAY}
        >
          <FilledImage src={imageSrc} />
        </Styles.DiamondContainer>
        <Column gap style={{ flex: 1, width: 0 }}>
          <Styles.Title>
            <strong>{title}</strong>
          </Styles.Title>
          <Styles.TextContainer gap>
            <div>{contents}</div>
            <div>{year}</div>
          </Styles.TextContainer>
        </Column>
        <Styles.Background color={'#fff'} />
      </Styles.Container>
    </Link>
  );
};
