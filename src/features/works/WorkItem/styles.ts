import { Column } from '@components/alignment/Column';
import { Row } from '@components/alignment/Row';
import { Diamond } from '@components/Shapes/Diamond';
import styled from 'styled-components';

export const DiamondContainer = styled(Diamond).withConfig({
  shouldForwardProp: (props) => !['borderColor'].includes(props),
})<{ borderColor: string }>((props) => ({
  '&::before': {
    content: "''",
    position: 'absolute',
    inset: '15%',
    transition: 'ease .4s',
    transform: 'rotate(45deg)',
    opacity: 0,
    zIndex: 2,
    border: `solid 2px ${props.borderColor}`,
  },
}));

export const Title = styled.div((props) => ({
  paddingTop: '1em',
  fontSize: '1.5em',
  position: 'relative',
  zIndex: 2,
  '& strong::before': {
    right: '100%',
    transition: 'ease .2s',
  },
}));

export const Container = styled(Row)((props) => ({
  width: 400,
  position: 'relative',
  cursor: 'pointer',
  padding: '.25em',
  zIndex: 2,
  '&:hover': {
    [` ${Title} strong::before`]: {
      right: 0,
    },
    [`>${DiamondContainer}::before`]: {
      inset: '14%',
      transform: 'rotate(38deg)',
      opacity: 1,
    },
  },
}));

export const TextContainer = styled(Column).withConfig({
  shouldForwardProp: (props) => ![].includes(props),
})<{}>(() => ({
  borderLeft: 'solid 2px currentColor',
  paddingLeft: '1em',
  '> div': {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    height: '1.5em',
  },
}));

export const Background = styled.div.withConfig({
  shouldForwardProp: (props) => !['color'].includes(props),
})<{}>((props) => ({
  zIndex: -1,
  position: 'absolute',
  inset: 0,
  right: '100%',
  transition: 'ease 1s',
  background: `linear-gradient(90deg,${props.color} ,transparent 100%)`,
  clipPath: 'polygon(0 50%, 77px 0%, 100% 0, 100% 100%, 77px 100%)',

  [`div:hover>&`]: {
    right: 0,
  },
}));
