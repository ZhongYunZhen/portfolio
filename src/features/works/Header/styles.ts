import styled from 'styled-components';

export const Header = styled.div.withConfig({})<{}>(() => ({
  padding: '1em 3em',
  position: 'sticky',
  top: 0,
  zIndex: 99,
}));
