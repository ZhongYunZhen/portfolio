import React from 'react';
import * as Styles from './styles';

export const Header: React.FC<React.PropsWithChildren<{}>> = ({ children }) => {
  return <Styles.Header>{children}</Styles.Header>;
};
