import { Row } from '@components/alignment/Row';
import styled from 'styled-components';

type DiamondTextProps = {
  color: string;
  fontSize: string | number;
};

export const DiamondText = styled(Row).withConfig({
  shouldForwardProp: (props) => !['color', 'fontSize'].includes(props),
})<DiamondTextProps>((props) => ({
  color: props.color,
  fontSize: props.fontSize,
}));
