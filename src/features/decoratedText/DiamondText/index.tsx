import React from 'react';
import { Diamond } from '@components/Shapes/Diamond';
import * as Styles from './styles';

interface DiamondTextProps extends React.HTMLAttributes<HTMLDivElement> {
  color: string;
  fill?: boolean;
  fontSize?: string | number;
}

export const DiamondText: React.FC<DiamondTextProps> = ({
  color,
  fontSize = '1.25em',
  fill = true,
  children,
}) => {
  return (
    <Styles.DiamondText align="center" color={color} fontSize={fontSize} gap>
      <Diamond fill={fill} color={color} />
      {children}
    </Styles.DiamondText>
  );
};
