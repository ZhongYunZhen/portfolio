import { Column } from '@components/alignment/Column';
import styled from 'styled-components';

type PageTitleContainerProps = {
  color?: string;
};

export const PageTitleContainer = styled.div.withConfig({
  shouldForwardProp: (props) => ![].includes(props),
})<PageTitleContainerProps>((props) => ({
  color: props.color,
  fontSize: '2em',
}));

export const TextContainer = styled(Column).withConfig({
  shouldForwardProp: (props) => ![].includes(props),
})<{}>((props) => ({
  position: 'relative',
  padding: '0 1em',
}));

export const DividingLine = styled.div.withConfig({
  shouldForwardProp: (props) => ![].includes(props),
})<{}>((props) => ({
  position: 'absolute',
  top: '50%',
  left: 0,
  right: 0,
  borderBottom: 'solid 3px currentColor',
}));

export const Text = styled.div.withConfig({
  shouldForwardProp: (props) => !['fontSize'].includes(props),
})<{ fontSize: string | number }>((props) => ({
  fontSize: props.fontSize,
}));
