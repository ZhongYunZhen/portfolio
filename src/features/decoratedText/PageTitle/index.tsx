import React from 'react';
import Head from 'next/head';
import { Row } from '@components/alignment/Row';
import { Diamond } from '@components/Shapes/Diamond';
import { THEME } from '@constants/theme';
import * as Styles from './styles';

interface PageTitleProps extends React.HTMLAttributes<HTMLDivElement> {
  title: string;
  subtitle?: string;
}

export const PageTitle: React.FC<PageTitleProps> = ({ title, subtitle, ...props }) => {
  return (
    <>
      <Head>
        <title>{title} | Yun-Zhen Zhong</title>
      </Head>
      <Styles.PageTitleContainer {...props}>
        <Row gap="0.25em" align="center" justify="center">
          <Diamond fill color={THEME.COLOR.TEXT} size="0.75em" />
          <Styles.TextContainer align="center">
            <Styles.Text fontSize="1em">{title}</Styles.Text>
            <Styles.DividingLine />
            <Styles.Text fontSize="0.75em" style={{ paddingTop: '0.25em' }}>
              {subtitle}
            </Styles.Text>
          </Styles.TextContainer>
          <Diamond fill color={THEME.COLOR.TEXT} size="0.75em" />
        </Row>
      </Styles.PageTitleContainer>
    </>
  );
};
