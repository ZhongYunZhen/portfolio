import * as CONSTANTS from './constants';
export const getEnableSize = () => {
  const value = (window.innerWidth - document.getElementById('body').offsetWidth) / 2;
  if (value > CONSTANTS.MAX_ENABLE_SIZE) return CONSTANTS.MAX_ENABLE_SIZE;
  if (value < CONSTANTS.MIN_ENABLE_SIZE) return CONSTANTS.MIN_ENABLE_SIZE;
  return value;
};
