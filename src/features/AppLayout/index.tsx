import React, { HTMLAttributes } from 'react';
import dynamic from 'next/dynamic';
import { Body } from '@components/layout/Body';
import { Container } from '@components/layout/Container';
import { Content } from '@components/layout/Content';
import { Footer } from '@components/layout/Footer';
import { Icons } from '@components/layout/Header';
import { Menu } from '@components/layout/Menu';
import * as utils from './utils';

const Decorations = dynamic(() => import('@features/Decorations'), { ssr: false });

export const AppLayout: React.FC<React.PropsWithChildren<HTMLAttributes<HTMLElement>>> = ({
  children,
}) => {
  return (
    <Container>
      <Decorations getEnableSize={utils.getEnableSize} />
      <Icons />
      <Body>
        <Menu />
        <Content>{children}</Content>
        <Footer />
      </Body>
    </Container>
  );
};
