import styled from 'styled-components';

export const Container = styled.div((props) => ({
  width: 200,
  height: 200,
  cursor: 'pointer',
  overflow: 'hidden',
  position: 'relative',
  border: 'solid 1px ' + props.color,
}));

export const ImageContent = styled.div<{ url: string }>((props) => ({
  background: `center/cover url(${props.url})`,
  transform: 'scale(1)',
  transition: 'ease .4s',
  position: 'absolute',
  inset: 0,
  [Container + ':hover > &']: {
    transform: 'scale(1.1)',
  },
}));
