import React from 'react';
import { THEME } from '@constants/theme';
import * as Styles from './styles';

interface GalleryItemProps extends React.HTMLAttributes<HTMLDivElement> {
  url: string;
}

export const GalleryItem: React.FC<GalleryItemProps> = ({ url, ...props }) => {
  return (
    <Styles.Container color={THEME.COLOR.GRAY} {...props}>
      <Styles.ImageContent url={url} />
    </Styles.Container>
  );
};
