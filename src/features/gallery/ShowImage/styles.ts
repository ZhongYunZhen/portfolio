import styled from 'styled-components';

export const StaticBackground = styled.div.withConfig({
  shouldForwardProp: (props) => !['backgroundColor'].includes(props),
})<{ backgroundColor: string }>((props) => ({
  position: 'fixed',
  inset: 0,
  backgroundColor: props.backgroundColor,
  zIndex: 99,
}));

export const ImageContent = styled.div.withConfig({})<{ url: string }>((props) => ({
  background: `center / contain no-repeat url(${props.url})`,
}));
