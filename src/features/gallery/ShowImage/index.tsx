import React, { useRef, useState } from 'react';
import { Column } from '@components/alignment/Column';
import { DisplayWall } from '@components/commons/DisplayWall';
import * as Styles from './styles';

interface ShowImageProps extends React.HTMLAttributes<HTMLDivElement> {
  imageList: string[];
  showImage: number;
  onHidden: React.MouseEventHandler;
}

export const ShowImage: React.FC<ShowImageProps> = ({ imageList, showImage, onHidden }) => {
  const [position, setPosition] = useState<number>(showImage);
  const ref = useRef();
  const handleBackgroundClick: React.MouseEventHandler = (e) => {
    if (e.target === ref.current) {
      onHidden(e);
    }
  };
  return (
    <Styles.StaticBackground backgroundColor="#00000088" onClick={handleBackgroundClick}>
      <Column
        ref={ref}
        align="center"
        justify="center"
        style={{ height: '100%' }}
        onClick={(e) => e.preventDefault()}
      >
        <DisplayWall
          list={imageList.map((src) => ({ type: 'image', src }))}
          position={position}
          width={860}
          height={460}
        />
        <DisplayWall.SelectBar
          list={imageList}
          position={position}
          onPositionChange={setPosition}
        />
      </Column>
    </Styles.StaticBackground>
  );
};
