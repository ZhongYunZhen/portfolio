/** the app layout type code */
export enum LayoutType {
  DEFAULT = 'DEFAULT',
  NO_LAYOUT = 'NO_LAYOUT',
}
