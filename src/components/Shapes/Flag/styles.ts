import styled from 'styled-components';

type FlagProps = {
  backgroundColor?: string;
  border?: string;
  color?: string;
  borderWidth?: string;
  height?: string;
};

export const Flag = styled('div').withConfig({
  shouldForwardProp: (prop) =>
    !['backgroundColor', 'border', 'color', 'borderWidth', 'height'].includes(prop),
})<FlagProps>((props) => ({
  position: 'relative',
  display: 'inline-block',
  padding: '0.5em 1.5em',
  color: props.color,
  backgroundColor: props.backgroundColor,
  marginLeft: '-1em',
  border: props.border ? `solid ${props.borderWidth} ${props.border}` : 'none',
  '&, &::before, &::after': {
    clipPath:
      `polygon(0% 0% ,` +
      `calc(100% - ${props.height}) 0%,` +
      `100% 50%,` +
      `calc(100% - ${props.height}) 100%,` +
      `0% 100%,` +
      `${props.height} 50%)`,
  },
  '&::before, &::after': {
    content: '""',
    position: 'absolute',
    inset: `-${props.borderWidth}`,
    width: `calc(${+props.borderWidth.split('px')[0] * 1.5}px + ${props.height})`,
    backgroundColor: props.border,
  },
  '&::before': {
    right: '100%',
  },
  '&::after': {
    right: '-' + props.borderWidth,
    left: 'auto',
  },
}));
