import React, { useMemo } from 'react';
import * as Styles from './styles';

interface FlagProps extends React.HTMLAttributes<HTMLDivElement> {
  color?: string;
  fill?: boolean;
  backgroundColor?: string;
  textColor?: string;
  border?: string;
  flagHeight?: string;
}

type ColorProps = {
  backgroundColor?: string;
  textColor?: string;
  border?: string;
};

export const Flag = React.forwardRef<HTMLDivElement, FlagProps>(
  ({ color, fill, children, backgroundColor, flagHeight = '1em', ...props }, ref) => {
    const colorProps = useMemo<ColorProps>(() => {
      if (fill) {
        return { backgroundColor: color, textColor: '#fff' };
      }
      return { textColor: color, backgroundColor };
    }, [color, fill, backgroundColor]);

    return (
      <Styles.Flag
        backgroundColor={colorProps.backgroundColor}
        color={colorProps.textColor}
        border={color}
        borderWidth="2px"
        height={flagHeight}
        ref={ref}
        {...props}
      >
        {children}
      </Styles.Flag>
    );
  }
);
