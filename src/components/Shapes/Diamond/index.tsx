import React, { useMemo } from 'react';
import * as Styles from './styles';

export interface DiamondProps extends React.HTMLAttributes<HTMLDivElement> {
  color?: string;
  textColor?: string;
  fill?: boolean;
  size?: string | number;
  borderWidth?: string | number;
  background?: string;
}

type ColorProps = {
  background?: string;
  textColor?: string;
  border?: string;
};

export const Diamond = React.forwardRef<HTMLDivElement, DiamondProps>(
  (
    { color, textColor, fill, size = '1em', children, borderWidth = '1px', background, ...props },
    ref
  ) => {
    const colorProps = useMemo<ColorProps>(() => {
      if (fill) {
        return { background: color, textColor: textColor || '#fff' };
      }
      return { border: color, textColor: color, background };
    }, [color, fill, background]);

    return (
      <Styles.Diamond
        background={colorProps.background}
        border={colorProps.border}
        color={colorProps.textColor}
        width={size}
        height={size}
        borderWidth={borderWidth}
        ref={ref}
        {...props}
      >
        <div>
          <div>{children}</div>
        </div>
      </Styles.Diamond>
    );
  }
);
