import styled from 'styled-components';

type DiamondProps = {
  background?: string;
  border?: string;
  color?: string;
  borderWidth?: string | number;
  width?: string | number;
  height?: string | number;
};

export const Diamond = styled('div').withConfig({
  shouldForwardProp: (prop) =>
    !['background', 'border', 'color', 'borderWidth', 'width', 'height'].includes(prop),
})<DiamondProps>((props) => ({
  position: 'relative',
  display: 'inline-block',
  width: props.width,
  height: props.height,
  color: props.color,
  '> div': {
    position: 'absolute',
    inset: '15%',
    transform: 'rotate(45deg)',
    background: props.background,
    border: props.borderWidth + ' solid ' + props.border,
    overflow: 'hidden',
    '> div': {
      position: 'absolute',
      transform: 'rotate(-45deg)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      inset: '-20%',
    },
  },
}));
