import styled from 'styled-components';

export const LinkButton = styled.div.withConfig({})<{}>(() => ({
  cursor: 'pointer',
}));

type TextButtonProps = {
  color: string;
};

export const TextButton = styled.div.withConfig({
  shouldForwardProp: (props) => !['color'].includes(props),
})<TextButtonProps>((props) => ({
  color: props.color,
  position: 'relative',
  '::after': {
    content: "''",
    position: 'absolute',
    inset: 0,
    borderBottom: 'solid 1.5px currentColor',
    left: '50%',
    right: '50%',
    transition: 'ease .2s',
  },
  ':hover::after': {
    left: '-0.25em',
    right: '-0.25em',
  },
}));
