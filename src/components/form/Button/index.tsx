import React from 'react';
import Link from 'next/link';
import * as Styles from './styles';

interface LinkButtonProp extends React.PropsWithChildren<{}> {
  href: string;
}

export const LinkButton: React.FC<LinkButtonProp> = ({ href, children }) => {
  return (
    <Link href={href}>
      <Styles.LinkButton>{children}</Styles.LinkButton>
    </Link>
  );
};

interface TextButtonProp extends React.HTMLAttributes<HTMLDivElement> {
  color?: string;
  href?: string;
}

export const TextButton: React.FC<TextButtonProp> = ({
  color = 'currenColor',
  href,
  children,
  ...props
}) => {
  if (href) {
    return (
      <LinkButton href={href}>
        <Styles.TextButton {...props} color={color}>
          {children}
        </Styles.TextButton>
      </LinkButton>
    );
  }
  return (
    <Styles.TextButton {...props} color={color}>
      {children}
    </Styles.TextButton>
  );
};
