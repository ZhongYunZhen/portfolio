import { Row } from '@components/alignment/Row';
import styled from 'styled-components';

export const Container = styled(Row).withConfig({
  shouldForwardProp: (props) => !['fontSize'].includes(props),
})<{ fontSize?: string | number }>((props) => ({ fontSize: props.fontSize }));

export const FlagContainer = styled.div((props) => ({
  filter: `drop-shadow(0.2em 0.3em 0 ${props.color})`,
}));
