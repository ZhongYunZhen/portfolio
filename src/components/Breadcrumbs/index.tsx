import React from 'react';
import { LinkButton, TextButton } from '@components/form/Button';
import { Diamond } from '@components/Shapes/Diamond';
import { Flag } from '@components/Shapes/Flag';
import { THEME } from '@constants/theme';
import * as Styles from './styles';

export type Path = {
  url?: string;
  text?: string;
  fill?: boolean;
};

export interface BreadcrumbsProps extends React.HTMLAttributes<HTMLDivElement> {
  mainColor?: string;
  secondColor?: string;
  fontSize?: string | number;
  path?: Path[];
}

export const Breadcrumbs: React.FC<BreadcrumbsProps> = ({
  mainColor,
  secondColor,
  path,
  fontSize,
  ...props
}) => {
  return (
    <Styles.Container fontSize={fontSize} gap="5px" align="center" {...props}>
      {path[0].url ? (
        <LinkButton href={path[0].url}>
          <Diamond fill color={mainColor || THEME.COLOR.HIGHT_LIGHT} size="2em" />
        </LinkButton>
      ) : (
        <Diamond fill color={mainColor || THEME.COLOR.HIGHT_LIGHT} size="2em" />
      )}
      <div />
      {path.slice(1).map(({ url, text, fill }) => (
        <Styles.FlagContainer color={mainColor || THEME.COLOR.HIGHT_LIGHT} key={url || ''}>
          <Flag
            color={secondColor || THEME.COLOR.DECO_B}
            backgroundColor={THEME.COLOR.BODY}
            fill={fill}
          >
            {url ? <TextButton href={url}>{text.toUpperCase()}</TextButton> : text.toUpperCase()}
          </Flag>
        </Styles.FlagContainer>
      ))}
    </Styles.Container>
  );
};
