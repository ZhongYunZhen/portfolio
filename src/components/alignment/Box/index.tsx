import React, { useMemo } from 'react';
import { booleanEscape } from '@utils/booleanEscape';
import { unit, UnitType } from '@styles/css/unit';
import * as Styles from './styles';
import * as CONSTANTS from './constants';

export interface BoxProps extends React.HTMLAttributes<HTMLDivElement> {
  direction: CSSProperties['flexDirection'];
  justify?: CSSProperties['justifyContent'];
  align?: CSSProperties['alignItems'];
  wrap?: CSSProperties['flexWrap'] | boolean;
  columnGap?: UnitType | boolean;
  rowGap?: UnitType | boolean;
  grow?: boolean | number;
  shrink?: boolean | number;
}

export const Box = React.forwardRef<HTMLDivElement, BoxProps>(
  (
    { direction, justify, align, wrap, columnGap, rowGap, grow, shrink, children, ...props },
    ref
  ) => {
    const flexWrap = useMemo(() => booleanEscape(wrap, 'wrap', 'nowrap'), [wrap]);
    const flexGrow = useMemo(() => booleanEscape(grow, 1, 0), [grow]);
    const flexShrink = useMemo(() => booleanEscape(shrink, 1, 0), [shrink]);
    const flexColumnGap = useMemo(
      () => booleanEscape(columnGap, CONSTANTS.DEFAULT_GAP_SIZE),
      [columnGap]
    );
    const flexRowGap = useMemo(() => booleanEscape(rowGap, CONSTANTS.DEFAULT_GAP_SIZE), [rowGap]);

    return (
      <Styles.Box
        {...props}
        ref={ref}
        justify={justify}
        align={align}
        columnGap={unit(flexColumnGap)}
        rowGap={unit(flexRowGap)}
        wrap={flexWrap}
        grow={flexGrow}
        shrink={flexShrink}
        direction={direction}
      >
        {children}
      </Styles.Box>
    );
  }
);
