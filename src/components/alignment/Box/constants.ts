import { UnitType } from 'src/styles/css/unit';

export const DEFAULT_GAP_SIZE: UnitType = { unit: 'em', value: 0.75 };
