import styled from 'styled-components';

type BoxExtendProps = {
  direction: CSSProperties['flexDirection'];
  align?: string;
  justify?: string;
  columnGap?: string;
  rowGap?: string;
  wrap?: CSSProperties['flexWrap'];
  grow?: number;
  shrink?: number;
};

export const Box = styled('div').withConfig({
  shouldForwardProp: (prop) =>
    !['direction', 'align', 'justify', 'columnGap', 'rowGap', 'wrap', 'grow', 'shrink'].includes(
      prop
    ),
})<BoxExtendProps>((props) => ({
  display: 'flex',
  flexDirection: props.direction,
  flexWrap: props.wrap,
  flexGrow: props.grow,
  flexShrink: props.shrink,
  justifyContent: props.justify,
  alignItems: props.align,
  columnGap: props.columnGap,
  rowGap: props.rowGap,
}));
