import React, { useMemo } from 'react';
import { UnitType } from '@styles/css/unit';
import { Box, BoxProps } from '../Box';

interface RowProps extends Omit<BoxProps, 'columnGap' | 'direction'> {
  gap?: UnitType | boolean;
  reversed?: boolean;
}

export const Row = React.forwardRef<HTMLDivElement, RowProps>(
  ({ children, gap, rowGap, reversed, ...props }, ref) => {
    const flexDirection = useMemo<CSSProperties['flexDirection']>(
      () => (reversed ? 'row-reverse' : 'row'),
      [reversed]
    );

    return (
      <Box {...props} ref={ref} direction={flexDirection} columnGap={gap} rowGap={rowGap}>
        {children}
      </Box>
    );
  }
);
