import React, { useMemo } from 'react';
import { UnitType } from '@styles/css/unit';
import { Box, BoxProps } from '../Box';

interface ColumnProps extends Omit<BoxProps, 'rowGap' | 'direction'> {
  gap?: UnitType | boolean;
  reversed?: boolean;
}

export const Column = React.forwardRef<HTMLDivElement, ColumnProps>(
  ({ children, gap, columnGap, reversed, ...props }, ref) => {
    const flexDirection = useMemo<CSSProperties['flexDirection']>(
      () => (reversed ? 'column-reverse' : 'column'),
      [reversed]
    );

    return (
      <Box {...props} ref={ref} direction={flexDirection} rowGap={gap} columnGap={columnGap}>
        {children}
      </Box>
    );
  }
);
