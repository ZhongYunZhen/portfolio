import { Row } from '@components/alignment/Row';
import styled, { keyframes } from 'styled-components';

type DisplayWallProps = {
  width: string | number;
  height: string | number;
};

export const DisplayWall = styled.div.withConfig({
  shouldForwardProp: (props) => !['width', 'height'].includes(props),
})<DisplayWallProps>((props) => ({
  width: props.width,
  height: props.height,
  overflow: 'hidden',
}));

type MovingWallProps = {
  width: string | number;
  height: string | number;
};

export const MovingWall = styled(Row).withConfig({
  shouldForwardProp: (props) => !['width', 'height'].includes(props),
})<MovingWallProps>((props) => ({
  width: props.width,
  height: props.height,
  '> div': {
    position: 'absolute',
    '&:nth-child(2)': {
      transform: 'translateX(100%)',
    },
  },
}));
