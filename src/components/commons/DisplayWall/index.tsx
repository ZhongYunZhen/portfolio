import React, { useEffect, useRef, useState } from 'react';
import { DisplayBox } from './DisplayBox';
import { SelectBar } from './SelectBar';
import * as Styles from './styles';

export type DisplayItem = {
  type: 'youtube' | 'image';
  src: string;
};

interface DisplayWallProps extends React.HTMLAttributes<HTMLDivElement> {
  width: string | number;
  height: string | number;
  list: DisplayItem[];
  position: number;
  children?: (displayImageData: { type: 'youtube' | 'image'; src: string }, index: number) => {};
}

export const DisplayWallBase: React.FC<DisplayWallProps> = ({
  width,
  height,
  list,
  position,
  children,
}) => {
  const [displayItems, setDisplayItem] = useState<DisplayItem[]>([list[position]]);
  const ref = useRef();
  const prePos = useRef<number>(position);

  useEffect(() => {
    setDisplayItem([list[position]]);
    return () => {
      prePos.current = position;
    };
  }, [position, list]);

  return (
    <Styles.DisplayWall width={width} height={height}>
      <Styles.MovingWall ref={ref} justify="center" width={width} height={height}>
        {!children &&
          displayItems.map(({ type, src }, i) => (
            <DisplayBox key={i} type={type} src={src} width={width} height={height} />
          ))}
        {children && displayItems.map(children)}
      </Styles.MovingWall>
    </Styles.DisplayWall>
  );
};

export const DisplayWall = Object.assign(DisplayWallBase, { DisplayBox, SelectBar });
