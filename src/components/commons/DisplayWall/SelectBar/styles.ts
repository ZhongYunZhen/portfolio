import { Diamond } from '@components/Shapes/Diamond';
import styled from 'styled-components';

type ButtonProps = {
  rotate: number;
};

export const Button = styled.div.withConfig({
  shouldForwardProp: (props) => !['rotate'].includes(props),
})<ButtonProps>((props) => ({
  clipPath: 'polygon(0 0 , 50% 0 , 50% 100% , 0 100% )',
  transform: `rotate(${props.rotate}deg)`,
  cursor: 'pointer',
}));

export const ImageContainer = styled(Diamond).withConfig({
  shouldForwardProp: (props) => !['pos', 'clickable'].includes(props),
})<{ pos: number; clickable: boolean }>((props) => ({
  position: 'absolute',
  cursor: props.clickable && 'pointer',
  transform: `translateX(${props.pos}px)`,
  transition: 'transform ease .5s, width linear .5s,height linear .5s',
}));
