import React, { useMemo } from 'react';
import { Row } from '@components/alignment/Row';
import { FilledImage } from '@components/layout/FilledImage';
import { Diamond } from '@components/Shapes/Diamond';
import { THEME } from '@constants/theme';
import * as Styles from './styles';

const NORMAL_SIZE = 50;

type SelectBarProps = {
  displayItemCount?: number;
  list: string[];
  position: number;
  onPositionChange: (pos: number) => void;
};

type ListItem = {
  src: string;
  id: number;
};

type DisplayItem = ListItem & { pos: number; size: number };

export const SelectBar: React.FC<SelectBarProps> = ({
  displayItemCount = 5,
  list,
  position,
  onPositionChange,
}) => {
  const filledList = useMemo<ListItem[]>(() => {
    const filledList = new Array(displayItemCount + 2).fill('');
    list.map((data, index) => {
      filledList[index] = data;
    });
    return filledList.map((src, id) => ({ src, id }));
  }, [list, displayItemCount]);

  const displayItemList = useMemo<DisplayItem[]>(() => {
    const fillNum = Math.floor(displayItemCount / 2) + 1;

    const currentList = [
      ...filledList.slice(-fillNum),
      ...filledList,
      ...filledList.slice(0, fillNum),
    ];

    const list = currentList.slice(position, position + displayItemCount + 2);

    return list.map((data, index) => {
      const centerPos = Math.floor(list.length / 2);
      const isCenter = index === Math.floor(list.length / 2);
      const isBorder = index === 0 || index === list.length - 1;
      const size = isCenter ? NORMAL_SIZE * 1.5 : isBorder ? 0 : NORMAL_SIZE;
      let pos = 0;
      if (index - centerPos > 0) pos = (index - centerPos + 0.25) * NORMAL_SIZE;
      if (index - centerPos < 0) pos = (index - centerPos - 0.25) * NORMAL_SIZE;

      return { pos, size, ...data };
    });
  }, [filledList, displayItemCount, position]);

  const getCurrentPos = (target: number): number => {
    if (target >= list.length) {
      return target % list.length;
    }
    if (target < 0) {
      return list.length - 1;
    }
    return target;
  };

  const handlePositionChange = (target: number) => () => {
    onPositionChange(getCurrentPos(target));
  };

  return (
    <Row justify="center" align="center" style={{ height: NORMAL_SIZE * 1.5 }}>
      {/* decrease pos button */}
      <div style={{ transform: `translateX(-${NORMAL_SIZE * (displayItemCount / 2 + 0.5)}px)` }}>
        <Styles.Button rotate={0} onClick={handlePositionChange(position - 1)}>
          <Diamond color={THEME.COLOR.DECO_B} fill size={NORMAL_SIZE / 2} />
        </Styles.Button>
      </div>

      {/* absolute pos button */}
      {displayItemList.map(({ src, id, pos, size }) => {
        return (
          <Styles.ImageContainer
            pos={pos}
            key={id}
            size={size}
            clickable={!!src}
            fill
            color={THEME.COLOR.GRAY}
            onClick={src ? handlePositionChange(id) : () => {}}
          >
            <FilledImage src={src} />
          </Styles.ImageContainer>
        );
      })}

      {/* increase pos button */}
      <div style={{ transform: `translateX(${NORMAL_SIZE * (displayItemCount / 2 + 0.5)}px)` }}>
        <Styles.Button rotate={180} onClick={handlePositionChange(position + 1)}>
          <Diamond color={THEME.COLOR.DECO_B} fill size={NORMAL_SIZE / 2} />
        </Styles.Button>
      </div>
    </Row>
  );
};
