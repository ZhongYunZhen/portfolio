import React from 'react';
import { THEME } from '@constants/theme';
import * as Styles from './styles';

interface DisplayBoxProps extends React.HTMLAttributes<HTMLDivElement> {
  type?: 'youtube' | 'image';
  width: string | number;
  height: string | number;
  src: string;
}

export const DisplayBox: React.FC<DisplayBoxProps> = ({ type, width, height, src, ...props }) => {
  let Component = ImageBlock;
  if (type === 'youtube') {
    Component = YTVideo;
  }

  return (
    <Styles.DisplayBox justify="center" width={width} height={height} color={THEME.COLOR.GRAY}>
      <Component src={src} />
    </Styles.DisplayBox>
  );
};

const ImageBlock: React.FC<{ src: string }> = ({ src }) => {
  return <img src={src} style={{ maxWidth: '100%', maxHeight: '100%' }} />;
};

const YTVideo: React.FC<{ src: string }> = ({ src }) => {
  return (
    <iframe
      src={src}
      title="YouTube video player"
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
      allowFullScreen
      style={{ width: '100%', height: '100%' }}
    />
  );
};
