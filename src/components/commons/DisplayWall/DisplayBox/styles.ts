import { Row } from '@components/alignment/Row';
import styled from 'styled-components';

type DisplayBoxProps = {
  width: string | number;
  height: string | number;
};

export const DisplayBox = styled(Row).withConfig({
  shouldForwardProp: (props) => !['width', 'height'].includes(props),
})<DisplayBoxProps>((props) => ({
  width: props.width,
  height: props.height,
  backgroundColor: props.color,
}));
