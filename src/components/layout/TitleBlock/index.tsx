import React from 'react';
import { Breadcrumbs, BreadcrumbsProps, Path } from '@components/Breadcrumbs';
import * as Styles from './styles';

interface TitleBlockProps extends BreadcrumbsProps {
  title: string;
  titlePath: Path[];
}

export const TitleBlock: React.FC<TitleBlockProps> = ({
  title,
  titlePath,
  children,
  style,
  ...props
}) => {
  return (
    <div style={style}>
      <Styles.TitleHolder id={title}>
        <Breadcrumbs {...props} path={titlePath} />
      </Styles.TitleHolder>
      {children}
    </div>
  );
};
