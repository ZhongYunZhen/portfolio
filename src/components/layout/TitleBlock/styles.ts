import styled from 'styled-components';

export const TitleHolder = styled.div((props) => ({
  padding: '1em 0',
  position: 'sticky',
  top: 0,
  zIndex: 99,
}));
