import React, { useState } from 'react';
import { menuContext } from './context';
import { MenuBlock } from './MenuBlock';
import { MenuButton } from './MenuButton';

export const Menu: React.FC<{}> = ({}) => {
  const [openMenu, setOpenMenu] = useState<boolean>(false);

  return (
    <menuContext.Provider value={{ openMenu, setOpenMenu }}>
      <MenuButton />
      {openMenu && <MenuBlock />}
    </menuContext.Provider>
  );
};
