import React, { useContext } from 'react';

const value: { openMenu: boolean; setOpenMenu: React.Dispatch<React.SetStateAction<boolean>> } = {
  openMenu: false,
  setOpenMenu: () => {},
};

export const menuContext = React.createContext(value);

export const useMenuStatus = () => {
  const { openMenu, setOpenMenu } = useContext(menuContext);
  return [openMenu, setOpenMenu] as const;
};
