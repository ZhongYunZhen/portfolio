import React from 'react';
import { Column } from '@components/alignment/Column';
import { Breadcrumbs } from '@components/Breadcrumbs';
import { THEME } from '@constants/theme';
import { PaddingBox } from '../PaddingBox';
import { MenuItemType } from './menuData';

const MENU_COLOR_PATTERN: { mainColor: string; secondColor: string }[] = [
  { mainColor: THEME.COLOR.DECO_C, secondColor: THEME.COLOR.MENU },
  { mainColor: THEME.COLOR.HIGHT_LIGHT, secondColor: THEME.COLOR.DECO_B },
];

type MenuItemProps = React.HTMLAttributes<HTMLDivElement> & MenuItemType & { colorCount?: number };

export const MenuItem: React.FC<MenuItemProps> = ({
  title,
  href,
  content,
  colorCount = 0,
  ...props
}) => {
  return (
    <>
      <Breadcrumbs
        path={[{}, { url: href, text: title }]}
        {...MENU_COLOR_PATTERN[colorCount]}
        {...props}
      />
      {content && (
        <PaddingBox paddingLeft="3em" paddingBottom="1em">
          <Column gap>
            {content.map((data) => (
              <MenuItem key={data.title} colorCount={colorCount + 1} {...data} {...props} />
            ))}
          </Column>
        </PaddingBox>
      )}
    </>
  );
};
