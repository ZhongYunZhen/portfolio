import React from 'react';
import { THEME } from '@constants/theme';
import { mixColor } from '@utils/mixColor';
import { Column } from '@components/alignment/Column';
import { menuData } from './menuData';
import { MenuItem } from './MenuItem';
import { useMenuStatus } from './context';
import * as Styles from './styles';

export const MenuBlock: React.FC<{}> = ({}) => {
  const [, setOpenMenu] = useMenuStatus();

  return (
    <Styles.Container>
      <Styles.Content backgroundColor={mixColor(THEME.COLOR.BODY, 'transparent', 0.5)}>
        <Column gap>
          {menuData.map((data) => (
            <MenuItem {...data} key={data.title} onClick={() => setOpenMenu(false)} />
          ))}
        </Column>
      </Styles.Content>
    </Styles.Container>
  );
};
