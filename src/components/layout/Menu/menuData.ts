import * as ROUTES from '@constants/routes';
import galleryData from '@static-source/gallery.json';
import workData from '@static-source/works.json';

export type MenuItemType = {
  title: string;
  href: string;
  content?: MenuItemType[];
};

const aboutPageData: MenuItemType = {
  title: 'About',
  href: ROUTES.ABOUT,
  content: [
    {
      title: 'Autobiography',
      href: ROUTES.ABOUT_ANCHOR.AUTOBIOGRAPHY,
    },
    {
      title: 'Experience',
      href: ROUTES.ABOUT_ANCHOR.EXPERIENCE,
    },
    {
      title: 'Skills',
      href: ROUTES.ABOUT_ANCHOR.SKILLS,
    },
  ],
};

const workPageData: MenuItemType = (() => {
  const base: MenuItemType = {
    title: 'Works',
    href: ROUTES.WORKS,
    content: [],
  };
  const typeMark: { [key: string]: string } = {};
  Object.keys(workData).map((name) => {
    const { tag } = workData[name];
    if (!typeMark[tag]) {
      base.content.push({ title: tag, href: ROUTES.WORKS + '#' + tag });
    }
    typeMark[tag] = tag;
  });

  return base;
})();

const galleryPageData: MenuItemType = (() => {
  const base: MenuItemType = {
    title: 'Gallery',
    href: ROUTES.GALLERY,
    content: [],
  };
  const typeMark: { [key: string]: string } = {};
  galleryData.map(({ type }) => {
    if (!typeMark[type]) {
      base.content.push({ title: type, href: ROUTES.GALLERY + '#' + type });
    }
    typeMark[type] = type;
  });

  return base;
})();

export const menuData: MenuItemType[] = [
  { title: 'Home', href: '/' },
  aboutPageData,
  workPageData,
  galleryPageData,
];
