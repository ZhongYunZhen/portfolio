import { Row } from '@components/alignment/Row';
import { Diamond } from '@components/Shapes/Diamond';
import styled from 'styled-components';

export const Container = styled.div((props) => ({
  position: 'fixed',
  inset: 0,
  zIndex: 100,
}));

export const MenuButton = styled(Diamond).withConfig({
  shouldForwardProp: (props) => !['size', 'openMenu'].includes(props),
})<{ openMenu: boolean; size: number | string }>((props) => ({
  position: 'fixed',
  zIndex: 101,
  transition: 'ease 1s',
  top: typeof props.size === 'number' ? (props.size / 2) * -1 : '1em',
  left: typeof props.size === 'number' ? (props.size / 2) * -1 : '90%',
  cursor: 'pointer',
  '@media (min-width: 980px)': {
    left: props.openMenu ? '-' + props.size : 'calc(50% + 400px)',
  },
}));

export const MenuContent = styled.div<{ openMenu: boolean; size: number }>((props) => ({
  position: 'absolute',
  inset: 0,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  '>div': {
    marginLeft: props.openMenu && props.size / 4,
    marginTop: props.openMenu && props.size / 2,
    width: props.openMenu && '0!important',
    '&::before, &::after': {
      content: "''",
    },
    '&, &::before, &::after': {
      position: 'absolute',
      width: props.openMenu ? '3em' : '2em',
      borderBottom: 'solid 2px ' + props.color,
      borderBottomWidth: props.openMenu && 4,
      transition: 'ease 1s',
    },
    '&::before': {
      marginTop: '.5em',
      transform: props.openMenu && 'translateY(-.5em) rotate(45deg)',
    },
    '&::after': {
      marginTop: '-.5em',
      transform: props.openMenu && 'translateY(.5em) rotate(-45deg)',
    },
  },
}));

export const MainButton = styled(Row).withConfig({
  shouldForwardProp: (props) => !['color', 'textColor'].includes(props),
})<{ color: string; textColor: string }>((props) => ({
  position: 'absolute',
  left: 0,
  top: 0,
  width: 200,
  height: 200,
  padding: 50,
  clipPath: 'polygon(0 0, 100% 0, 0 100%)',
  color: props.textColor,
  fontSize: 30,
  backgroundColor: props.color,
}));

export const Content = styled.div.withConfig({
  shouldForwardProp: (props) => !['backgroundColor'].includes(props),
})<{ backgroundColor: string }>((props) => ({
  position: 'absolute',
  inset: 0,
  zIndex: -1,
  overflow: 'auto',
  backdropFilter: 'blur(15px)',
  background: props.backgroundColor,
  '>div': {
    paddingTop: 200,
    paddingLeft: '15%',
    paddingBottom: 150,
    '@media (min-width: 980px)': {
      paddingTop: 150,
      paddingLeft: '30%',
    },
  },
}));
