import React from 'react';
import { THEME } from '@constants/theme';
import { useMenuStatus } from './context';
import * as Styles from './styles';

export const MenuButton: React.FC<{}> = ({}) => {
  const [openMenu, setOpenMenu] = useMenuStatus();

  return (
    <Styles.MenuButton
      color={THEME.COLOR.MENU}
      openMenu={openMenu}
      onClick={() => setOpenMenu((openMenu) => !openMenu)}
      size={openMenu ? 500 : '5em'}
      fill
    >
      <Styles.MenuContent color={THEME.COLOR.BODY} openMenu={openMenu} size={300}>
        <div />
      </Styles.MenuContent>
    </Styles.MenuButton>
  );
};
