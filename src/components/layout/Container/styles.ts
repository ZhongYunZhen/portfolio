import styled from 'styled-components';

type ContainerProps = {
  backgroundColor: string;
  hightLightColor: string;
  color: string;
};

export const Container = styled.div.withConfig({
  shouldForwardProp: (props) => !['backgroundColor', 'color', 'hightLightColor'].includes(props),
})<ContainerProps>((props) => ({
  backgroundColor: props.backgroundColor,
  color: props.color,
  width: '100%',
  '& strong': {
    position: 'relative',
    zIndex: 2,
    fontWeight: 'normal',
    '::before': {
      content: "''",
      position: 'absolute',
      inset: 0,
      zIndex: -1,
      top: '50%',
      backgroundColor: props.hightLightColor,
    },
  },
}));
