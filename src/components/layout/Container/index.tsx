import React from 'react';
import { THEME } from '@constants/theme';
import * as Styles from './styles';

export const Container: React.FC<React.PropsWithChildren<{}>> = ({ children }) => {
  return (
    <Styles.Container
      backgroundColor={THEME.COLOR.BODY}
      color={THEME.COLOR.TEXT}
      hightLightColor={THEME.COLOR.HIGHT_LIGHT}
    >
      {children}
    </Styles.Container>
  );
};
