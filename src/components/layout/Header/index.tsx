import React from 'react';
import Head from 'next/head';

export const Icons: React.FC = () => {
  return (
    <Head>
      <link rel="apple-touch-icon" sizes="57x57" href="/icon.ico/apple-icon-57x57.png" />
      <link rel="apple-touch-icon" sizes="60x60" href="/icon.ico/apple-icon-60x60.png" />
      <link rel="apple-touch-icon" sizes="72x72" href="/icon.ico/apple-icon-72x72.png" />
      <link rel="apple-touch-icon" sizes="76x76" href="/icon.ico/apple-icon-76x76.png" />
      <link rel="apple-touch-icon" sizes="114x114" href="/icon.ico/apple-icon-114x114.png" />
      <link rel="apple-touch-icon" sizes="120x120" href="/icon.ico/apple-icon-120x120.png" />
      <link rel="apple-touch-icon" sizes="144x144" href="/icon.ico/apple-icon-144x144.png" />
      <link rel="apple-touch-icon" sizes="152x152" href="/icon.ico/apple-icon-152x152.png" />
      <link rel="apple-touch-icon" sizes="180x180" href="/icon.ico/apple-icon-180x180.png" />
      <link rel="icon" type="image/png" sizes="192x192" href="/icon.ico/android-icon-192x192.png" />
      <link rel="icon" type="image/png" sizes="32x32" href="/icon.ico/favicon-32x32.png" />
      <link rel="icon" type="image/png" sizes="96x96" href="/icon.ico/favicon-96x96.png" />
      <link rel="icon" type="image/png" sizes="16x16" href="/icon.ico/favicon-16x16.png" />
      <meta name="msapplication-TileImage" content="/icon.ico/ms-icon-144x144.png" />
      <link rel="manifest" href="/icon.ico/manifest.json" />
      <meta name="msapplication-TileColor" content="#ffffff" />
      <meta name="theme-color" content="#ffffff" />
    </Head>
  );
};
