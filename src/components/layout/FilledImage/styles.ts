import styled from 'styled-components';

export const FilledImage = styled.div.withConfig({
  shouldForwardProp: (props) => !['src'].includes(props),
})<{ src: string }>((props) => ({
  position: 'absolute',
  inset: 0,
  background: `center / cover url(${props.src})`,
}));
