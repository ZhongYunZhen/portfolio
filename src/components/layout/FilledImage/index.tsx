import React from 'react';
import * as Styles from './styles';

interface FilledImageType extends React.HTMLAttributes<HTMLDivElement> {
  src: string;
}

export const FilledImage: React.FC<FilledImageType> = ({ children, ...props }) => {
  return <Styles.FilledImage {...props}>{children}</Styles.FilledImage>;
};
