import React from 'react';
import { PaddingBox } from '../PaddingBox';
import * as Styles from './styles';

export interface BodyProps extends React.HTMLAttributes<HTMLDivElement> {}

export const Body: React.FC<React.PropsWithChildren<BodyProps>> = ({ children }) => {
  return (
    <Styles.Body gap id="body">
      <PaddingBox paddingTop="1em" />
      {children}
    </Styles.Body>
  );
};
