import { Column } from '@components/alignment/Column';
import styled from 'styled-components';

export const Body = styled(Column)(() => ({
  minHeight: '100vh',
  margin: 'auto',
  width: '90%',
  display: 'flex',
  '@media (min-width: 980px)': {
    width: '900px',
  },
}));
