import React from 'react';
import * as Styles from './styles';

interface FooterProps extends React.HTMLAttributes<HTMLDivElement> {}

export const Footer: React.FC<FooterProps> = () => {
  return (
    <Styles.Footer gap="2em" justify="center" wrap color="#ccc" rowGap="1em">
      <span>版權所有 © 2023 Yun-Zhen Zhong</span>
      <span>Copyright © 2023 Yun-Zhen Zhong all rights reserved</span>
    </Styles.Footer>
  );
};
