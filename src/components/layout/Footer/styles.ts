import { Row } from '@components/alignment/Row';
import styled from 'styled-components';

type FooterProps = {
  color?: string;
  borderColor?: string;
  textColor?: string;
};

export const Footer = styled(Row).withConfig({
  shouldForwardProp: (prop) => !['color', 'borderColor', 'textColor'].includes(prop),
})<FooterProps>((props) => ({
  borderTop: `solid 1px ${props.borderColor ?? props.color}`,
  color: props.textColor ?? props.color,
  textAlign: 'center',
  padding: '1em 0 2em',
  width: '100%',
  fontSize: 'small',
  whiteSpace: 'pre-wrap',
  margin: '50px auto 0 auto',
}));
