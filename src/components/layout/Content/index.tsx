import React from 'react';
import * as Styles from './styles';

export const Content = ({ children }) => {
  return <Styles.Content>{children}</Styles.Content>;
};
