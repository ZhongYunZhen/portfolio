import React from 'react';
import * as Styles from './styles';

interface PaddingBoxProps extends React.HTMLAttributes<HTMLDivElement> {
  padding?: string | number;
  paddingTop?: string | number;
  paddingBottom?: string | number;
  paddingLeft?: string | number;
  paddingRight?: string | number;
}

export const PaddingBox: React.FC<PaddingBoxProps> = ({ children, ...props }) => {
  return <Styles.PaddingBox {...props}>{children}</Styles.PaddingBox>;
};
