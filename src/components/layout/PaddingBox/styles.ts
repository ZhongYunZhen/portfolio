import styled from 'styled-components';

type PaddingBoxProps = {
  padding?: string | number;
  paddingTop?: string | number;
  paddingBottom?: string | number;
  paddingLeft?: string | number;
  paddingRight?: string | number;
};

export const PaddingBox = styled.div.withConfig({
  shouldForwardProp: (props) =>
    !['padding', 'paddingTop', 'paddingBottom', 'paddingLeft', 'paddingRight'].includes(props),
})<PaddingBoxProps>((props) => ({
  padding: props.padding,
  paddingTop: props.paddingTop,
  paddingBottom: props.paddingBottom,
  paddingLeft: props.paddingLeft,
  paddingRight: props.paddingRight,
}));
