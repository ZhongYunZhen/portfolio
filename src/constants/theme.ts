export const THEME = {
  COLOR: {
    DECO_A: '#3756a7',
    DECO_B: '#3dbfdc',
    DECO_C: '#47eacc',
    MENU: '#1f84bd',
    BODY: '#f4f4f4',
    HIGHT_LIGHT: '#ffdb7d',
    TEXT: '#555555',
    GRAY: '#d9d9d9',
  },
};
