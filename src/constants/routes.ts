export const HOME = '/';

export const ABOUT = '/about';

export const ABOUT_ANCHOR = {
  AUTOBIOGRAPHY: `${ABOUT}#autobiography`,
  EXPERIENCE: `${ABOUT}#experience`,
  SKILLS: `${ABOUT}#skills`,
};

export const WORKS = '/works';

export const GALLERY = '/gallery';
