declare type CSSProperties = Completed<import('csstype').Properties>;

type DecodeRef<T extends React.Ref<any>> = T extends React.Ref<infer V> ? V : never

declare type GetPropRef<C extends React.ComponentType<any> | keyof JSX.IntrinsicElements> = DecodeRef<NonNullable<React.ComponentProps<C>['ref']>>

