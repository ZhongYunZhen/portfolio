import { NextPage } from 'next';
import { LayoutType } from '@enums/LayoutType';

export type CustomNextPage<P = {}, IP = P> = NextPage<P, IP> & {
  layout?: LayoutType | React.ComponentType<any>;
};
