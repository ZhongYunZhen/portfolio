type CommonValue = (string & {}) | (number & {}) | { unit: CommonUnit; value: string | number };
type CommonUnit = keyof typeof commonUnits;

export type UnitType = CommonValue | CommonValue[];

const generateUnit = (suffix: string) => {
  const unit = (value: UnitType | undefined | null): string | undefined => {
    if (value === null || value === undefined) return;

    if (Array.isArray(value)) return value.map(unit).join(' ');

    if (typeof value === 'object') return auto(value);

    return typeof value === 'string' ? value : `${value}${suffix}`;
  };

  return unit;
};

const commonUnits = {
  px: generateUnit('px'),
  em: generateUnit('em'),
  rem: generateUnit('rem'),
  vw: generateUnit('vw'),
  vh: generateUnit('vh'),
  percent: generateUnit('%'),
};

const auto = (value: UnitType | undefined | null, defineUnit?: CommonUnit): string | undefined => {
  if (value === null || value === undefined) return;

  if (defineUnit) return commonUnits[defineUnit](value);

  if (Array.isArray(value)) return value.map((v) => auto(v)).join(' ');

  if (typeof value === 'object') return commonUnits[value.unit](value.value);

  return commonUnits.px(value);
};

export const unit = Object.assign(auto, { ...commonUnits, auto });
